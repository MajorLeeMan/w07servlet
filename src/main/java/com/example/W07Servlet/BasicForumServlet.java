package com.example.W07Servlet;

/*
For this servlet, these are our objectives:
Takes a response from the web page.
Process the request.
Respond with another webpage or response.

In the case of a forum, our program API will do the following:
Collect a string from a textbox on the webpage
Collect whether the text is a reply to another comment or a new comment entirely
Insert the text back onto the webpage as HTML code.
Refresh the page for the user, with their post present on the page.
*/

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;


@WebServlet(name = "basicForum", value = "/basic-forum")
public class BasicForumServlet extends HttpServlet {

    public void doPost(HttpServletRequest request)
            throws IOException {
        //process post if submitted by user
        System.out.println("Entered Write method...");
        String postText = request.getParameter("textfield");
        System.out.println(postText);
        //if the user has posted text, add that in before displaying.
        FileWriter writer = new FileWriter("forumposts.txt", true);
        BufferedWriter buff = new BufferedWriter(writer);
        buff.newLine();
        buff.write(postText);
        buff.close();
    }

    //this method executes when the client requests the servlet
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        System.out.println("Entered doGet method...");
        response.setContentType("text/html");

        //check for a file, if not there, create a file to store these forum 'posts'
        File forum = new File("forumposts.txt");
        if (forum.createNewFile()) {
            System.out.println("File didn't exist, creating new file.");
        } else {
            System.out.println("File exists. Reading...");
        }
        Scanner fileinput = new Scanner(forum);

        //When the servlet is run, this helps construct what the user sees.
        PrintWriter out = response.getWriter(); //PrintWriter object to hold the response
        out.println("<html><body>"); //the beginning of the html document
        //out.println("<h1>" + message + "</h1>");
        out.println("<h1>" + "Welcome to the Forum" + "</h1>");


        out.println("<p><form name=\"postform\"  method=\"doPost\">" + //action="/basic-forum"
                //add an option to add posts
                "<input type=\"text\" name=\"textfield\" value=\"\" maxlength=\"100\">" +
                //present a button to the user called "post to forum"
                "</p><input type=\"Submit\" value=\"Post to Forum!\"></form><br>");
        //out.println("<button type = \"button\">Post to forum!</button>");

        out.println("<h2>Forum posts are below.</h2><br>");

        //read from the file once it's created/found
        System.out.println("Scanning "+forum);

        //create an ArrayList from the file that holds text and displays the
        //  entire list to the user (use the ArrayList method)
        //create the object array
        ArrayList<String> posts = new ArrayList<String>();
        System.out.println("Created posts List.");

        System.out.println("Within the while loop...");
        while (fileinput.hasNextLine()) {
            //put the input into the post variable and add it into the ArrayList
            String post = fileinput.nextLine();
            posts.add(post);
        }

        System.out.println("Finished reading.");
        fileinput.close(); //close the read
        System.out.println("Displaying posts List...");
        for (int i=0; i<posts.size(); i++)
        {
            //print the Array posts
            out.println("<p>"+posts.get(i)+"</p>");
        }
        System.out.println("Finished.");

        //close the html properly
        out.println("</body></html>");
    }

    //destroy the... servlet? Connection?
    public void destroy() {
        System.out.println("Destroy?");
    }
}
