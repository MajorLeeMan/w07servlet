<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/> <!--Break.-->
<a href="basic-forum">Click here to go to the forum</a> <!--Link to redirect to the servlet.-->
</body>
</html>